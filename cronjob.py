from sqlalchemy.sql import text
from swirl import engine
from dateutil.relativedelta import relativedelta

import datetime

today = datetime.date.today()

if today.day == 16:
    pp = today - relativedelta(days=15)
else:
    pp = today - relativedelta(months=1) + relativedelta(days=15)

pp_str = "%s-%s-%s" % (pp.year, pp.month, pp.day)

engine.execute(text("INSERT INTO HR_PayPeriod (PayPeriod) VALUES (:PayPeriod)"), PayPeriod=pp_str)