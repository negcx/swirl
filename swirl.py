from flask import Flask, request, g, session, render_template, flash, redirect, url_for, jsonify, make_response
from hashlib import sha1
from functools import wraps
import json
import csv
import datetime
import time
import os
import re
import sys
import pymysql
import version

import traceback

from dateutil.parser import parse as dt_parse
from dateutil.relativedelta import relativedelta

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from werkzeug import secure_filename

from ctuit_constants import *

# During transition process, import pyodbc and sqlalchemy so that both can be used
import sqlalchemy
from sqlalchemy.sql import select, text
from sqlalchemy import and_
import pymysql

# Connect to SQLAlchemy
if not version.remote:
	engine = sqlalchemy.create_engine('mysql+pymysql://kyle:ff4e9fdk@166.78.5.108/blendbarrel?charset=latin1',
		echo=True)
else:
	# Recycle connection pool every hour 
	engine = sqlalchemy.create_engine('mysql+pymysql://swirl:swirl@localhost/blendbarrel?charset=latin1',
		echo=False, pool_recycle=3600, pool_size=10)
meta = sqlalchemy.MetaData()
meta.bind = engine
meta.reflect()

def easy_hash(s):
	return sha1(s).hexdigest()

# UPLOAD SETTINGS
#UPLOAD_FOLDER = 'C:/projects/swirl/uploads'
ALLOWED_EXTENSIONS = set(['csv', 'txt'])

def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

# Initialize Flask framework
app = Flask(__name__)

if version.debug:
	app.config.update(
		SECRET_KEY = 'development key',
		DEBUG = True
		)
else:
	app.config.update(
		SECRET_KEY = '2835238fdkja823523asdf',
		DEBUG = False
		)

app.jinja_env.filters['sha1'] = easy_hash

##### DECORATORS ######

def admin_access(uid):
	if uid in [1,3]:
		return True
	else:
		return False

def admin_only(f):
	@wraps(f)
	def decorated_function(*args, **kwargs):
		if not 'UserID' in session:
			session['next_url'] = request.url
			return redirect(url_for('login'))
		elif not admin_access(session['UserID']):
			flash('Access denied for user ID %s.' % session['UserID'], 'alert-error')
			return redirect(url_for('index'))
		else:
			session.pop('next_url', None)
		return f(*args, **kwargs)

	return decorated_function

def login_required(f):
	@wraps(f)
	def decorated_function(*args, **kwargs):
		if not 'UserID' in session:
			session['next_url'] = request.url
			return redirect(url_for('login'))
		else:
			session.pop('next_url', None)
		return f(*args, **kwargs)

	return decorated_function

class MetaTablesProxy():
	def __init__(self, meta):
		self.meta = meta

	def __getattr__(self, name):
		return self.meta.tables[name]

##### FILTERS ######

@app.before_request
def before_request():
	global engine
	global meta

	g.engine = engine
	g.meta = meta
	g.t = MetaTablesProxy(meta)

@app.teardown_request
def teardown_request(exception):
	pass

##### JINJA FILTERS #####
@app.template_filter('currency')
def currency_filter(s):
	return '${:,.2f}'.format(s)

@app.template_filter('decimal')
def decimal_filter(s):
	return '{:,.2f}'.format(s)



##### HELPER FUNCTIONS #####
def send_system_mail(to, subject, body):
	msg = MIMEText(body, 'html')
	msg['Subject'] = subject
	msg['From'] = 'systems@vinovolo.com'
	msg['To'] = to

	s = smtplib.SMTP('smtp.googlemail.com', 587)
	s.ehlo()
	s.starttls()
	s.ehlo()
	s.login('systems@vinovolo.com', '1vinovolo')
	s.sendmail('systems@vinovolo.com', to, msg.as_string())
	s.quit()

##### ADMIN - users ######

@app.route('/admin/users')
@login_required
@admin_only
def users_handler():
	return render_template('users.html',
		users=g.meta.tables['User'].select().order_by(meta.tables['User'].c.UserID).execute().fetchall())

@app.route('/admin/users/deactivate/<int:uid>')
@login_required
@admin_only
def users_deactivate(uid):
	g.t.User.update().where(g.t.User.c.UserID==uid).values(Active=0).execute()

	flash("User %d deactivated successfully." % uid, 'alert-success')

	return redirect(url_for('users_handler'))

@app.route('/admin/users/activate/<int:uid>')
@login_required
@admin_only
def users_activate(uid):
	g.t.User.update().where(g.t.User.c.UserID==uid).values(Active=1).execute()

	flash("User %d activated successfully." % uid, 'alert-success')

	return redirect(url_for('users_handler'))

@app.route('/admin/users/delete/<int:uid>')
@login_required
@admin_only
def users_delete(uid):
	g.meta.tables['User'].delete().where(g.meta.tables['User'].c.UserID==uid).execute()

	flash("User deleted successfully.", "alert-success")

	return redirect(url_for('users_handler'))

@app.route('/admin/users/add', methods=['GET', 'POST'])
@login_required
@admin_only
def users_add():
	if request.method == 'GET':
		return render_template('user_add.html')

	elif request.method == 'POST':
		try:
			hashed_password = sha1(request.form['Email'] + request.form['Password']).hexdigest()

			g.t.User.insert().values(Email=request.form['Email'], Password=hashed_password).execute()

		except:
			flash("Unable to create User %s.  User may already exist." % request.form['Email'], "alert-error")
			return render_template('user_add.html')

		flash("User %s created successfully." % request.form['Email'], "alert-success")
		return redirect(url_for('users_handler'))

@app.route('/admin/users/edit/<int:uid>', methods=['GET', 'POST'])
@login_required
@admin_only
def users_edit(uid):
	if request.method == 'GET':
		return render_template('user_edit.html', user=g.meta.tables['User'].select().where(g.meta.tables['User'].c.UserID==uid).execute().fetchone())

	elif request.method == 'POST':
		try:
			hashed_password = sha1(request.form['Email'] + request.form['Password']).hexdigest()
			g.t.User.update().values(Email=request.form['Email'], Password=hashed_password).where(g.t.User.c.UserID==uid).execute()

		except:
			flash("Unable to update User %s." % request.form['Email'], "alert-error")
			return render_template('user_edit.html',
				user=g.t.User.select().where(g.t.User.c.UserID==uid).execute().fetchone())

		flash("User %s updated successfully." % request.form['Email'], "alert-success")
		return redirect(url_for('users_handler'))



##### LOGIN ######

@app.route('/login', methods=['GET', 'POST'])
def login():
	if request.method == 'POST':
		hashed_password = sha1(request.form['Email'] + request.form['Password']).hexdigest()
		
		conn = g.engine.connect()

		result = conn.execute(text('SELECT UserID FROM User WHERE Email = :email AND Password = :password AND Active >= 1'),
			email=request.form['Email'], password=hashed_password)

		if result.rowcount > 0:
			row = result.fetchone()

			session['UserID'] = row['UserID']
			session['admin_access'] = admin_access(row['UserID'])
			session['Email'] = request.form['Email']
			flash("You have successfully logged in.", "alert-success")
			if 'next_url' in session:
				return redirect(session['next_url'])
			else:
				return redirect(url_for('index'))
		else:
			flash("Invalid Username or password.", "alert-error")
			session.pop('UserID', None)
			session.pop('Email', None)

	return render_template('login.html')

@app.route('/logout')
def logout():
	session.pop('UserID', None)
	session.pop('Email', None)
	flash("You have successfully logged out.", "alert-success")
	return redirect(url_for('login'))



##### INDEX ######

@app.route('/')
@login_required
def index():
	
	PayPeriod = g.engine.execute(text('SELECT PayPeriod FROM HR_PayPeriod ORDER BY PayPeriod DESC LIMIT 1')).fetchone()['PayPeriod']

	query = text(
		"""SELECT s.StoreID, s.CtuitStoreID, s.Name,
			SUM(cp.Hours) AS Punches,
			SUM(t.Tips) AS Tips
			FROM Store s
		LEFT JOIN
			(SELECT StoreID, SUM(Hours) AS Hours FROM HR_CleanPunch
				WHERE PayPeriod = :PayPeriod
				GROUP BY StoreID) cp ON cp.StoreID = s.StoreID

		LEFT JOIN HR_Tips t ON t.StoreID = s.StoreID
			AND t.PayPeriod = :PayPeriod

		WHERE s.Active = 1

		

		GROUP BY s.StoreID
		ORDER BY s.StoreID
		""")

	stores = g.engine.execute(query, PayPeriod=PayPeriod).fetchall()

	return render_template('index.html', PayPeriod=PayPeriod, stores=stores)


##### SWIRL - CTUIT IMPORT ######

@app.route('/swirl/ctuit_import', methods=['POST'])
@login_required
def swirl_ctuit_import():
	f = request.files['import']

	# Create a new batch
	batch_id = batch_new(f.filename)

	try:
		# Build ctuit -> swirl store mapping lookup table
		stores = g.engine.execute(text(
			"""SELECT StoreID, CtuitStoreID FROM Store WHERE CtuitStoreID IS NOT NULL AND CtuitStoreID != ''
			""")).fetchall()

		ctuit_to_swirl = {}

		for store in stores:
			ctuit_to_swirl[store['CtuitStoreID']] = store['StoreID']

		if f and allowed_file(f.filename):
			csv_file = csv.reader(f.stream, delimiter=',', quotechar='"')

			# Loop through the file and add to list
			punch_list = []

			# Skip the first row which has headers
			first_row = True
			for row in csv_file:
				if first_row:
					first_row = False
					continue

				# Check to make sure this Ctuit store ID is in the lookup list
				if row[CTUIT_STORE_ID] not in ctuit_to_swirl:
					raise Exception("There's no Ctuit store mapping for %s. Please update the mapping in Maintenance->Stores." % row[CTUIT_STORE_ID])

				punch_list.append(
					{
					'BatchID': batch_id,
					'StoreID': ctuit_to_swirl[row[CTUIT_STORE_ID]],
					'EmployeeNum': row[CTUIT_EMPLOYEE_ID],
					'LastName': row[CTUIT_EMPLOYEE_NAME],
					'FirstName': '',
					'Department': row[CTUIT_DEPARTMENT],
					'Job': row[CTUIT_JOBNAME],
					'Date': sqldate(row[CTUIT_DATE]),
					'PunchIn': row[CTUIT_PUNCH_IN],
					'PunchOut': row[CTUIT_PUNCH_OUT],
					'Hours': row[CTUIT_HOURS]
					})

			g.engine.execute(g.t.HR_DirtyPunch.insert(), punch_list)
			g.t.HR_PunchBatch.update().values(Status='Dirty').where(g.t.HR_PunchBatch.c.BatchID==batch_id).execute()

			return redirect(url_for('import_preview', BatchID=batch_id))

		else:
			raise Exception

	except:
		g.t.HR_DirtyPunch.delete().where(g.t.HR_DirtyPunch.c.BatchID==batch_id).execute()
		g.t.HR_PunchBatch.delete().where(g.t.HR_PunchBatch.c.BatchID==batch_id).execute()

		flash("Unable to process %s.  Invalid file or wrong file type." % f.filename, "alert-error")
		flash("%s.  %s" % (sys.exc_info()[0], sys.exc_info()[1]), "alert-error")
		return redirect(url_for('swirl_import'))



##### SWIRL - IMPORT ######

@app.route('/swirl/tips_import', methods=['POST'])
def tips_import():
	f = request.files['import']

	# Pay periods
	pp_start = dt_parse(request.form['PayPeriod'])
	pp_end = None
	if pp_start.day == 1:
		pp_end = pp_start + datetime.timedelta(days=15)
	elif pp_start.day == 16:
		pp_end = pp_start + relativedelta(months=1) - relativedelta(days=16)


	# CSV file columns
	COL_STORE_ID = 0
	COL_TENDER = 2
	COL_BASE = 6 # Where cash tips live
	COL_TIP = 8
	COL_DATE = 3

	# These are the values of COL_TENDER when there are tips in BASE
	TENDER_TIP_NAMES = ['CASH TIPS', 'CASH TIP', 'US $ TIPS', 'TIPS ARGENT']

	# Create a dictionary of all the Ctuit store IDs
	stores = g.engine.execute('SELECT CtuitStoreID, StoreID FROM Store ORDER BY CtuitStoreID').fetchall()

	tips_dict = {}

	for store in stores:
		tips_dict[store['CtuitStoreID']] = {}
		tips_dict[store['CtuitStoreID']]['StoreID'] = store['StoreID']
		tips_dict[store['CtuitStoreID']]['tips'] = 0.0

	try:
		if f and allowed_file(f.filename):
			csv_file = csv.reader(f.stream, delimiter=",", quotechar='"')

			first_row = True

			# Read the file and parse the tips
			for row in csv_file:
				if first_row:
					first_row = False
					continue
				if dt_parse(row[COL_DATE]) >= pp_start and dt_parse(row[COL_DATE]) <= pp_end:
					tips_dict[row[COL_STORE_ID]]['tips'] += float(row[COL_TIP])
					if row[COL_TENDER] in TENDER_TIP_NAMES:
						tips_dict[row[COL_STORE_ID]]['tips'] += abs(float(row[COL_BASE]))

			# Create an insert list
			insert_list = []
			for key in tips_dict.keys():
				insert_list.append(
					{
					'StoreID': tips_dict[key]['StoreID'],
					'PayPeriod': request.form['PayPeriod'],
					'Tips': tips_dict[key]['tips']
					})

			
			conn = g.engine.connect()
			try:
				trans = conn.begin()

				# Delete existing tips in this pay period
				conn.execute(text('DELETE FROM HR_Tips WHERE PayPeriod = :PayPeriod'), PayPeriod=request.form['PayPeriod'])
				conn.execute(g.t.HR_Tips.insert(), insert_list)

				trans.commit()
				conn.close()
			except:
				trans.rollback()
				raise

			flash('Import successful!', 'alert-success')
			return redirect(url_for('tips_pp', PayPeriod=request.form['PayPeriod']))
		else:
			raise

	except:
		flash("Unable to process %s.  Invalid file or wrong file type." % f.filename, "alert-error")
		flash("%s.  %s" % (sys.exc_info()[0], sys.exc_info()[1]), "alert-error")

		traceback.print_tb(sys.exc_info()[2])

		return redirect(url_for('tips'))

@app.route('/swirl/import', methods=['GET', 'POST'])
@login_required
def swirl_import():
	if request.method == 'GET':
		return render_template('import_punches.html',
			stores=g.t.Store.select().where(g.t.Store.c.Active==1).order_by(g.t.Store.c.StoreID).execute())

	elif request.method == 'POST':
		f = request.files['import']

		# Create a new batch
		batch_id = batch_new(request.form['StoreID'] + ' - ' + f.filename)

		try:
			# Use the international date format if the user clicks the
			# international import button
			date_conversion = sqldate_intl if request.form['import_type'] == 'international' else sqldate

			if f and allowed_file(f.filename):
				csv_file = csv.reader(f.stream, delimiter=',', quotechar='"')

				# Loop through the file and add to list
				punch_list = []
				for row in csv_file:
					punch_list.append(
						{
						'BatchID': batch_id,
						'StoreID': request.form['StoreID'],
						'EmployeeNum': row[3],
						'LastName': row[4],
						'FirstName': row[5],
						'Department': row[6],
						'Job': row[7],
						'Date': date_conversion(row[8]),
						'PunchIn': sqltime(row[9]),
						'PunchOut': sqltime(row[10]),
						'Hours': row[11]
						})

				g.engine.execute(g.t.HR_DirtyPunch.insert(), punch_list)
				g.t.HR_PunchBatch.update().values(Status='Dirty').where(g.t.HR_PunchBatch.c.BatchID==batch_id).execute()

				return redirect(url_for('import_preview', BatchID=batch_id))

			else:
				raise Exception

		except:
			g.t.HR_DirtyPunch.delete().where(g.t.HR_DirtyPunch.c.BatchID==batch_id).execute()
			g.t.HR_PunchBatch.delete().where(g.t.HR_PunchBatch.c.BatchID==batch_id).execute()

			flash("Unable to process %s.  Invalid file or wrong file type." % f.filename, "alert-error")
			flash("%s.  %s" % (sys.exc_info()[0], sys.exc_info()[1]), "alert-error")
			return redirect(url_for('swirl_import'))

@app.route('/swirl/import/preview/<int:BatchID>')
@login_required
def import_preview(BatchID):
	punches = g.t.HR_DirtyPunch.select().where(g.t.HR_DirtyPunch.c.BatchID==BatchID).order_by(
		g.t.HR_DirtyPunch.c.EmployeeNum.desc(),
		g.t.HR_DirtyPunch.c.Date,
		g.t.HR_DirtyPunch.c.PunchIn).execute()

	employees = g.t.HR_DirtyPunch.select().where(g.t.HR_DirtyPunch.c.BatchID==BatchID).group_by(
		g.t.HR_DirtyPunch.c.EmployeeNum, g.t.HR_DirtyPunch.c.StoreID).order_by(g.t.HR_DirtyPunch.c.EmployeeNum.desc()).execute()

	return render_template('punch_preview.html', rows=punches, employees=employees, BatchID=BatchID)

@app.route('/swirl/import/update_employee_num/<int:BatchID>', methods=['POST'])
@login_required
def update_employee_num(BatchID):
	# store information about the employee
	
	(emp_num, emp_store) = request.form['OldEmployeeNum'].split('|')

	ee = g.engine.execute(text("""SELECT StoreID, LastName, FirstName
		FROM HR_DirtyPunch WHERE EmployeeNum=:OldEmployeeNum AND BatchID=:BatchID AND StoreID=:StoreID LIMIT 1"""),
		OldEmployeeNum=emp_num, StoreID=emp_store, BatchID=BatchID).fetchone()

	# keep track of change in HR_EmployeeNumUpdate table
	g.t.HR_EmployeeNumUpdate.insert().values(
		BatchID=BatchID,
		StoreID=ee['StoreID'],
		OldEmployeeNum=emp_num,
		NewEmployeeNum=request.form['NewEmployeeNum'],
		LastName=ee['LastName'],
		FirstName=ee['FirstName']).execute()

	# Update dirty punches with new employee number
	query = text("""UPDATE HR_DirtyPunch SET EmployeeNum=:NewEmployeeNum
		WHERE EmployeeNum=:OldEmployeeNum AND BatchID=:BatchID""")
	g.engine.execute(query,
		NewEmployeeNum=request.form['NewEmployeeNum'],
		OldEmployeeNum=emp_num,
		BatchID=BatchID)

	flash("Employee number %s changed to %s successfully." % (emp_num, request.form['NewEmployeeNum']), "alert-success")
	return redirect(url_for('import_preview', BatchID=BatchID))

def sqltime(s):
	try:
		return time.strftime("%H:%M:%S", time.strptime(s.strip(), "%I:%M:%S%p"))
	except ValueError:
		return time.strftime("%H:%M:%S", time.strptime(s.strip(), "%I:%M %p"))

def sqldate(s):
	m, d, y = s.split('/')
	return ("%s-%s-%s" % (y, m, d))

# Convert international formatted date to SQL formatted date
def sqldate_intl(s):
	d, m, y = s.split('/')
	return ("%s-%s-%s" % (y, m, d))

def batch_new(source):
	conn = g.engine.connect()

	query = text("""INSERT INTO HR_PunchBatch
		(UserID, Source,Timestamp,Status)
		VALUES (:UserID,:Source,NOW(),:Status)""")

	conn.execute(query,
		UserID=session['UserID'],
		Source=source,
		Status='New')

	batch_id = conn.execute('SELECT LAST_INSERT_ID() AS id').fetchone()

	return batch_id['id']

@app.route('/swirl/batches')
@login_required
def batches():
	return redirect(url_for('batches_page', page=1))

@app.route('/swirl/batches/<int:page>')
@login_required
def batches_page(page):
	if page < 1:
		return redirect(url_for('batches_page', page=1))

	query = text("""SELECT a.BatchID, b.Email, a.Source, a.Timestamp, a.Status FROM HR_PunchBatch a
		JOIN User b ON a.UserID = b.UserID
		ORDER BY a.BatchID DESC
		LIMIT :limit OFFSET :offset""")

	batches = g.engine.execute(query, limit=10, offset=(page-1)*10)

	return render_template('batches.html', batches=batches, page=page)


@app.route('/swirl/batch/map/<int:BatchID>')
@login_required
def batch_map(BatchID):
	# Find out which jobs haven't been mapped in the DB for this Store
	query = text("""SELECT dp.Job, dp.StoreID FROM HR_DirtyPunch dp WHERE BatchID = :BatchID AND Job NOT IN
		(SELECT SquirrelJob FROM HR_JobMap WHERE StoreID = dp.StoreID)
		GROUP BY Job, StoreID
		ORDER BY StoreID, Job""")

	result = g.engine.execute(query, BatchID=BatchID)

	# Save the number of unmapped jobs
	rowcount = result.rowcount

	# Only show the mapping screen if there are unmapped jobs
	if rowcount > 0:
		flash("There were jobs in the punch report that haven't been mapped.")

		jobs = g.t.HR_Job.select().order_by(g.t.HR_Job.c.JobID).execute().fetchall()

		return render_template('batch_map.html',
			unmapped_jobs=result.fetchall(), jobs=jobs, BatchID=BatchID)
	
	# No unmapped jobs, move on to processing
	else:
		return redirect(url_for('batch_process', BatchID=BatchID))

@app.route('/swirl/batch/process/<int:BatchID>')
@login_required
def batch_process(BatchID):
	# delete employees from dirty where employee ID is over 900000
	g.t.HR_DirtyPunch.delete().where(g.t.HR_DirtyPunch.c.EmployeeNum>=900000).execute()

	# delete from update table where NEW employee ID is over 900000
	g.t.HR_EmployeeNumUpdate.delete().where(g.t.HR_EmployeeNumUpdate.c.NewEmployeeNum>=900000).execute()

	# Try to send an email to the stores to tell them which employees to update in their system
	try:
		result = g.engine.execute(text(
			"""SELECT s.Name AS StoreName, enu.OldEmployeeNum, enu.NewEmployeeNum,
			enu.LastName, enu.Firstname FROM HR_EmployeeNumUpdate enu
			JOIN Store s ON s.StoreID = enu.StoreID
			WHERE enu.BatchID=:BatchID"""), BatchID=BatchID)

		if result.rowcount > 0:
			ee_updates = result.fetchall()

			email = g.engine.execute(text(
				"""SELECT s.Email FROM HR_PunchBatch pb JOIN Store s ON pb.StoreID = s.StoreID
				WHERE pb.BatchID = :BatchID"""), BatchID=BatchID).fetchone()['Email']

			send_system_mail(email, 'Employee Number Updates', render_template('ee_number_update_email.html', ee_updates=ee_updates))
	except:
		flash('There was an error trying to send the updated employee numbers via email.', 'alert-warning')

	# Clean up by deleting these from the update table
	g.t.HR_EmployeeNumUpdate.delete().where(g.t.HR_EmployeeNumUpdate.c.BatchID==BatchID).execute()


	# Copy punches from dirty to clean while mapping jobs properly
	query = text(
		"""INSERT INTO HR_CleanPunch (BatchID, StoreID, EmployeeNum, LastName, FirstName, JobID,
			Date, PunchIn, PunchOut, Hours)
				SELECT a.BatchID, a.StoreID, a.EmployeeNum, a.LastName, a.FirstName, b.JobID,
				a.Date, a.PunchIn, a.PunchOut, a.Hours FROM HR_DirtyPunch a
				JOIN HR_JobMap b ON a.Job = b.SquirrelJob AND a.StoreID = b.StoreID
				WHERE a.BatchID = :batch_id""")

	g.engine.execute(query, batch_id=BatchID)

	# Delete punches from dirty
	g.t.HR_DirtyPunch.delete().where(g.t.HR_DirtyPunch.c.BatchID==BatchID).execute()

	# Update batch
	g.t.HR_PunchBatch.update().where(g.t.HR_PunchBatch.c.BatchID==BatchID).values(
		Status='Clean').execute()

	flash("Batch processed successfully.", "alert-success")
	return redirect(url_for('batches'))

@app.route('/swirl/batch/delete/<int:BatchID>')
@login_required
def batch_delete(BatchID):
	g.t.HR_DirtyPunch.delete().where(g.t.HR_DirtyPunch.c.BatchID==BatchID).execute()
	g.t.HR_CleanPunch.delete().where(g.t.HR_CleanPunch.c.BatchID==BatchID).execute()
	g.t.HR_EmployeeNumUpdate.delete().where(g.t.HR_EmployeeNumUpdate.c.BatchID==BatchID).execute()
	g.t.HR_PunchBatch.delete().where(g.t.HR_PunchBatch.c.BatchID==BatchID).execute()

	flash("Batch deleted.")
	return redirect(url_for('batches'))

@app.route('/swirl/export', methods=['GET', 'POST'])
@login_required
def export():
	if request.method == 'GET':
		companies = g.engine.execute(text("SELECT * FROM Company WHERE Active=1 ORDER BY CompanyID")).fetchall()

		payperiods = g.t.HR_PayPeriod.select().order_by(g.t.HR_PayPeriod.c.PayPeriod.desc()).execute().fetchall()

		return render_template('export.html', companies=companies, payperiods=payperiods)
	else:
		return redirect(url_for('export_preview', CompanyID=request.form['CompanyID'],
			PayPeriod=request.form['PayPeriod']))

@app.route('/swirl/export/preview/<CompanyID>/<PayPeriod>')
@login_required
def export_preview(CompanyID, PayPeriod):
	
	# Check the company to see which overtime rules should be used
	company = g.t.Company.select().where(g.t.Company.c.CompanyID==CompanyID).execute().fetchone()

	# Federal overtime rules
	if company['OvertimeRules'] == 'Federal':
		query = text(
	"""SELECT
		  d.StoreID,
		  d.EmployeeNum,
		  d.LastName,
		  d.FirstName,
		  d.PayPeriod,
		  HR_Job.Name AS JobName,
		  SUM(d.Hours) AS Hours,
		  SUM(d.Overtime) AS Overtime,
		  SUM(d.Doubletime) AS Doubletime,
		  COALESCE(s.Costcenter, '') AS Costcenter,
		  COALESCE(HR_Job.Ratecode,'') AS Ratecode
		FROM
		    (SELECT
		       StoreID,
		       Week,
		       PayPeriod,
		       EmployeeNum,
		       FirstName,
		       LastName,
		       Date,
		       JobID,
		       CASE -- Calculate regular hours
		         WHEN Date IN (SELECT Holiday FROM HR_Holiday WHERE HolidayScheduleID = (SELECT HolidayScheduleID FROM Company WHERE CompanyID=:CompanyID)) -- Holiday
		         THEN
		           0
		         WHEN (HoursBefore < 40.0) AND
		              (HoursAfter >= 40)
		         THEN
		           (40.0 - HoursBefore)
		         WHEN (HoursBefore >= 40.0)
		         THEN
		           0
		         ELSE
		           Hours
		       END
		         AS Hours,
		       CASE -- Calculate overtime hours
		         WHEN (HoursBefore < 40.0) AND
		              (HoursAfter >= 40) AND
		              (Date IN (SELECT Holiday FROM HR_Holiday WHERE HolidayScheduleID = (SELECT HolidayScheduleID FROM Company WHERE CompanyID=:CompanyID))) -- Holiday
		         THEN
		           (40 - HoursBefore) -- The rest of the OT will be DT
		         WHEN (HoursBefore >= 40.0) AND
		              (Date IN (SELECT Holiday FROM HR_Holiday WHERE HolidayScheduleID = (SELECT HolidayScheduleID FROM Company WHERE CompanyID=:CompanyID))) -- Holiday
		         THEN
		           0 -- OT on a holiday should all be DT
		         WHEN (HoursBefore < 40.0) AND
		              (HoursAfter >= 40)
		         THEN
		           (HoursAfter - 40.0)
		         WHEN (HoursBefore >= 40.0) OR
		              (Date IN (SELECT Holiday FROM HR_Holiday WHERE HolidayScheduleID = (SELECT HolidayScheduleID FROM Company WHERE CompanyID=:CompanyID))) -- Regular OT or Holiday hours
		         THEN
		           Hours
		         ELSE
		           0
		       END
		         AS Overtime,
		       CASE -- Calculate doubletime hours
		         WHEN (HoursBefore < 40.0) AND
		              (HoursAfter >= 40) AND
		              (Date IN (SELECT Holiday FROM HR_Holiday WHERE HolidayScheduleID = (SELECT HolidayScheduleID FROM Company WHERE CompanyID=:CompanyID))) -- Holiday
		         THEN
		           (HoursAfter - 40)
		         WHEN (HoursBefore >= 40.0) AND
		              (Date IN (SELECT Holiday FROM HR_Holiday WHERE HolidayScheduleID = (SELECT HolidayScheduleID FROM Company WHERE CompanyID=:CompanyID))) -- Holiday
		         THEN
		           Hours -- OT on a holiday should all be DT
		         ELSE
		           0
		       END
		         AS Doubletime
		     FROM
		       (SELECT
		          a.EmployeeNum,
		          a.StoreID,
		          a.Week,
		          a.PayPeriod,
		          a.Date,
		          a.FirstName,
		          a.LastName,
		          a.JobID,
		          COALESCE(
		            SUM(b.Hours),
		            0)
		            AS HoursBefore,
		          a.Hours,
		          COALESCE(
		            SUM(b.Hours),
		            0) +
		          a.Hours
		            AS HoursAfter
		        FROM
		          (SELECT z1.* FROM HR_CleanPunch z1 JOIN Store u1 ON z1.StoreID = u1.StoreID WHERE u1.CompanyID = :CompanyID AND z1.Date >= DATE_SUB(:PayPeriod, INTERVAL WEEKDAY(:PayPeriod) DAY)) AS a
		          LEFT OUTER JOIN (SELECT z2.* FROM HR_CleanPunch z2 JOIN Store u2 ON z2.StoreID = u2.StoreID WHERE u2.CompanyID = :CompanyID AND z2.Date >= DATE_SUB(:PayPeriod, INTERVAL WEEKDAY(:PayPeriod) DAY)) AS b
		            ON (a.EmployeeNum = b.EmployeeNum) AND
		               (a.Week = b.Week) AND
		               (DATE_ADD(
		                  b.Date,
		                  INTERVAL b.PunchIn HOUR_SECOND) <
		                  DATE_ADD(
		                    a.Date,
		                    INTERVAL a.PunchIn HOUR_SECOND))
		        WHERE
		          a.Week >= WEEK(
		                      :PayPeriod, -- PARAMETER 2
		                      3)
		        GROUP BY
		          a.Week,
		          a.EmployeeNum,
		          a.Date,
		          a.JobID,
		          a.Hours,
		          a.PunchIn
		        ORDER BY
		          a.Week,
		          a.EmployeeNum,
		          a.Date,
		          a.PunchIn) AS c
		     ORDER BY
		       Week,
		       EmployeeNum,
		       Date,
		       JobID) AS d
		  JOIN
		    HR_Job
		  ON HR_Job.JobID = d.JobID
		  JOIN
		  	Store s
		  ON d.StoreID = s.StoreID
		WHERE
		  PayPeriod = :PayPeriod -- PARAMETER 3
		  AND EmployeeNum NOT IN
		  (SELECT EmployeeNum FROM HR_TerminatedEmployee WHERE CompanyID=:CompanyID)
		GROUP BY
		  d.StoreID,
		  PayPeriod,
		  EmployeeNum,
		  d.JobID
		ORDER BY
		  EmployeeNum
				""")

	# California overtime rules
	elif company['OvertimeRules'] == 'California':
		query = text(
		"""SELECT
	  d.StoreID,
	  d.EmployeeNum,
	  d.LastName,
	  d.FirstName,
	  d.PayPeriod,
	  HR_Job.Name AS JobName,
	  SUM(d.Hours) AS Hours,
	  SUM(d.Overtime + d.DailyOvertime) AS Overtime,
	  SUM(d.Doubletime + d.DailyDoubletime) AS Doubletime,
	  COALESCE(s.Costcenter, '') AS Costcenter,
	  COALESCE(HR_Job.Ratecode,'') AS Ratecode
	FROM
	    (SELECT
	       StoreID,
	       Week,
	       PayPeriod,
	       EmployeeNum,
	       FirstName,
	       LastName,
	       Date,
	       JobID,
	       CASE -- Calculate regular hours
	         WHEN (HoursBefore < 40.0) AND (HoursAfter >= 40) THEN (40.0 - HoursBefore)
			 WHEN (HoursBefore >= 40.0) THEN 0
	         ELSE Hours
	       END
	         AS Hours,
	       CASE -- Calculate overtime hours
	         WHEN (HoursBefore < 40.0) AND (HoursAfter >= 40) THEN (HoursAfter - 40.0)
	         WHEN (HoursBefore >= 40.0) THEN Hours
			 ELSE 0
	       END
	         AS Overtime,
	       0 AS Doubletime,
		   DailyOvertime,
		   DailyDoubletime
	     FROM
	       (SELECT
	          a.EmployeeNum,
	          a.StoreID,
	          a.Week,
	          a.PayPeriod,
	          a.Date,
	          a.FirstName,
	          a.LastName,
	          a.JobID,
	          COALESCE(SUM(b.Hours),0) AS HoursBefore,
	          a.Hours,
	          COALESCE(SUM(b.Hours),0) + a.Hours AS HoursAfter,
			  a.DailyOvertime,
			  a.DailyDoubletime
	        FROM
	          (SELECT z1.* FROM
			  
				(SELECT
				StoreID,
				Week,
				PayPeriod,
				EmployeeNum,
				FirstName,
				LastName,
				Date,
				PunchIn,
				PunchOut,
				JobID,
				CASE -- Calculate regular hours
					WHEN Date IN (SELECT Holiday FROM HR_Holiday WHERE HolidayScheduleID = (SELECT HolidayScheduleID FROM Company WHERE CompanyID=:CompanyID)) THEN 0 -- Holiday
					WHEN (HoursBefore < 8) AND (HoursAfter > 8) THEN (8 - HoursBefore)
					WHEN (HoursBefore >= 8) THEN 0
					ELSE Hours
				END
					AS Hours,
				CASE -- Calculate overtime hours
					WHEN Date IN (SELECT Holiday FROM HR_Holiday WHERE HolidayScheduleID = (SELECT HolidayScheduleID FROM Company WHERE CompanyID=:CompanyID)) -- Holiday Case
					THEN
						CASE
							WHEN HoursAfter <= 8 THEN Hours
							WHEN HoursAfter > 8 AND HoursBefore < 8 THEN 8 - HoursBefore
						END
					
					WHEN (HoursBefore <= 8) AND (HoursAfter > 8) AND (HoursAfter < 12) THEN (HoursAfter - 8)
					WHEN (HoursBefore <= 8) AND (HoursAfter > 8) THEN (8 - HoursBefore) -- The rest of the OT will be DT
					WHEN (HoursBefore <= 8) AND (HoursAfter > 8) AND (HoursAfter < 12) THEN (HoursAfter - 8)
					WHEN (HoursBefore >= 8 AND HoursAfter < 12) THEN Hours
					WHEN (HoursBefore >= 8 AND HoursAfter >= 12) THEN (12 - HoursBefore)
					ELSE 0
				END
					AS DailyOvertime,
				CASE -- Calculate doubletime hours
					WHEN Date IN (SELECT Holiday FROM HR_Holiday WHERE HolidayScheduleID = (SELECT HolidayScheduleID FROM Company WHERE CompanyID=:CompanyID)) -- Holiday Case
					THEN
						CASE
							WHEN HoursAfter <= 8 THEN 0
							WHEN HoursAfter > 8 AND HoursBefore < 8 THEN HoursAfter - 8
							WHEN HoursBefore > 8 THEN Hours
						END
					WHEN (HoursBefore < 12) AND (HoursAfter > 12) THEN (HoursAfter - 12)
					WHEN (HoursBefore >= 12) THEN Hours 
					ELSE 0
				END
					AS DailyDoubletime
				FROM
				(SELECT
					ca1_a.EmployeeNum,
					ca1_a.StoreID,
					ca1_a.Week,
					ca1_a.PayPeriod,
					ca1_a.Date,
					ca1_a.PunchIn,
					ca1_a.PunchOut,
					ca1_a.FirstName,
					ca1_a.LastName,
					ca1_a.JobID,
					COALESCE(SUM(ca1_b.Hours),0) AS HoursBefore,
					ca1_a.Hours,
					COALESCE(SUM(ca1_b.Hours),0) + ca1_a.Hours AS HoursAfter
					FROM
					(SELECT z1.* FROM HR_CleanPunch z1 JOIN Store u1 ON z1.StoreID = u1.StoreID WHERE u1.CompanyID = :CompanyID  AND z1.Date >= DATE_SUB(:PayPeriod, INTERVAL WEEKDAY(:PayPeriod) DAY)) AS ca1_a -- PARAMETER
					LEFT OUTER JOIN (SELECT z2.* FROM HR_CleanPunch z2 JOIN Store u2 ON z2.StoreID = u2.StoreID WHERE u2.CompanyID = :CompanyID  AND z2.Date >= DATE_SUB(:PayPeriod, INTERVAL WEEKDAY(:PayPeriod) DAY)) AS ca1_b -- PARAMETER
						ON (ca1_a.EmployeeNum = ca1_b.EmployeeNum) AND
						(ca1_a.Date = ca1_b.Date) AND
						(DATE_ADD(ca1_b.Date,INTERVAL ca1_b.PunchIn HOUR_SECOND) < DATE_ADD(ca1_a.Date,INTERVAL ca1_a.PunchIn HOUR_SECOND))
					WHERE
					ca1_a.Week >= WEEK(:PayPeriod,3) -- PARAMETER
					GROUP BY
					ca1_a.Week,
					ca1_a.EmployeeNum,
					ca1_a.Date,
					ca1_a.JobID,
					ca1_a.Hours,
					ca1_a.PunchIn
					ORDER BY
					ca1_a.Week,
					ca1_a.EmployeeNum,
					ca1_a.Date,
					ca1_a.PunchIn) AS ca1_c
				ORDER BY
				EmployeeNum,
				Date,
				PunchIn,
				StoreID,
				JobID) z1
	   		JOIN Store u1 ON z1.StoreID = u1.StoreID WHERE u1.CompanyID = :CompanyID) AS a -- PARAMETER
	          LEFT OUTER JOIN (SELECT z2.* FROM
			  
			    (SELECT
				StoreID,
				Week,
				PayPeriod,
				EmployeeNum,
				FirstName,
				LastName,
				Date,
				PunchIn,
				PunchOut,
				JobID,
				CASE -- Calculate regular hours
					WHEN Date IN (SELECT Holiday FROM HR_Holiday WHERE HolidayScheduleID = (SELECT HolidayScheduleID FROM Company WHERE CompanyID=:CompanyID)) THEN 0 -- Holiday
					WHEN (HoursBefore < 8) AND (HoursAfter >= 8) THEN (8 - HoursBefore)
					WHEN (HoursBefore >= 8) THEN 0
					ELSE Hours
				END
					AS Hours
				FROM
				(SELECT
					ca2_a.EmployeeNum,
					ca2_a.StoreID,
					ca2_a.Week,
					ca2_a.PayPeriod,
					ca2_a.Date,
					ca2_a.PunchIn,
					ca2_a.PunchOut,
					ca2_a.FirstName,
					ca2_a.LastName,
					ca2_a.JobID,
					COALESCE(SUM(ca2_b.Hours),0) AS HoursBefore,
					ca2_a.Hours,
					COALESCE(SUM(ca2_b.Hours),0) + ca2_a.Hours AS HoursAfter
					FROM
					(SELECT z1.* FROM HR_CleanPunch z1 JOIN Store u1 ON z1.StoreID = u1.StoreID WHERE u1.CompanyID = :CompanyID  AND z1.Date >= DATE_SUB(:PayPeriod, INTERVAL WEEKDAY(:PayPeriod) DAY)) AS ca2_a -- PARAMETER
					LEFT OUTER JOIN (SELECT z2.* FROM HR_CleanPunch z2 JOIN Store u2 ON z2.StoreID = u2.StoreID WHERE u2.CompanyID = :CompanyID  AND z2.Date >= DATE_SUB(:PayPeriod, INTERVAL WEEKDAY(:PayPeriod) DAY)) AS ca2_b -- PARAMETER
						ON (ca2_a.EmployeeNum = ca2_b.EmployeeNum) AND
						(ca2_a.Date = ca2_b.Date) AND
						(DATE_ADD(ca2_b.Date,INTERVAL ca2_b.PunchIn HOUR_SECOND) < DATE_ADD(ca2_a.Date,INTERVAL ca2_a.PunchIn HOUR_SECOND))
					WHERE
					ca2_a.Week >= WEEK(:PayPeriod,3) -- PARAMETER
					GROUP BY
					ca2_a.Week,
					ca2_a.EmployeeNum,
					ca2_a.Date,
					ca2_a.JobID,
					ca2_a.Hours,
					ca2_a.PunchIn
					ORDER BY
					ca2_a.Week,
					ca2_a.EmployeeNum,
					ca2_a.Date,
					ca2_a.PunchIn) AS ca2_c
				ORDER BY
				EmployeeNum,
				Date,
				PunchIn,
				StoreID,
				JobID) z2
	   		JOIN Store u2 ON z2.StoreID = u2.StoreID WHERE u2.CompanyID = :CompanyID) AS b -- PARAMETER
	            ON (a.EmployeeNum = b.EmployeeNum) AND
	               (a.Week = b.Week) AND
	               (DATE_ADD(b.Date,INTERVAL b.PunchIn HOUR_SECOND) < DATE_ADD(a.Date,INTERVAL a.PunchIn HOUR_SECOND))
	        WHERE
	          a.Week >= WEEK(:PayPeriod,3) -- PARAMETER
	        GROUP BY
	          a.Week,
	          a.EmployeeNum,
	          a.Date,
	          a.JobID,
	          a.PunchIn,
	          a.Hours
	        ORDER BY
	          a.Week,
	          a.EmployeeNum,
	          a.Date,
	          a.PunchIn) AS c
	     ORDER BY
	       Week,
	       EmployeeNum,
	       Date,
	       JobID) AS d
	  JOIN
	    HR_Job
	  ON HR_Job.JobID = d.JobID
	  JOIN
	  	Store s
	  ON d.StoreID = s.StoreID
	WHERE
	  PayPeriod = :PayPeriod -- PARAMETER
	  AND EmployeeNum NOT IN
		  (SELECT EmployeeNum FROM HR_TerminatedEmployee WHERE CompanyID=:CompanyID)
	GROUP BY
	  d.StoreID,
	  PayPeriod,
	  EmployeeNum,
	  d.JobID
	ORDER BY
	  EmployeeNum
			""")

	hours = g.engine.execute(query, CompanyID=CompanyID, PayPeriod=PayPeriod).fetchall()

	query = text(
	"""SELECT cp.EmployeeNum, s.StoreID, s.Costcenter, COALESCE(t.Tips * cp.TipShare, 0) AS Tips FROM Store s
	-- SELECT from Store as we need rows for each employee that worked for each store in the tip share

		JOIN
			(SELECT mcp.EmployeeNum, ms.TipPoolID,
				-- Determine the employee's share of tips by taking the employee's total tipable
				-- hours within the tip pool's group of stores divided by all employee's total tipable
				-- hours within the tip pool's group of stores
				(
				-- Employee hours
				SELECT SUM(emp.Hours) FROM HR_CleanPunch emp
					JOIN HR_Job ON emp.JobID = HR_Job.JobID
					JOIN Store ON emp.StoreID = Store.StoreID
					WHERE Store.TipPoolID = ms.TipPoolID
					AND emp.PayPeriod = mcp.PayPeriod
					AND HR_Job.Tipped = 1
					AND emp.EmployeeNum = mcp.EmployeeNum
					AND emp.EmployeeNum NOT IN
						(SELECT EmployeeNum FROM HR_TerminatedEmployee WHERE CompanyID=:CompanyID))
					
					/
				-- Total hours
				(SELECT SUM(ttl.Hours) FROM HR_CleanPunch ttl
					JOIN HR_Job ON ttl.JobID = HR_Job.JobID
					JOIN Store ON ttl.StoreID = Store.StoreID
					WHERE Store.TipPoolID = ms.TipPoolID
					AND ttl.PayPeriod = mcp.PayPeriod
					AND HR_Job.Tipped = 1
					AND ttl.EmployeeNum NOT IN
						(SELECT EmployeeNum FROM HR_TerminatedEmployee WHERE CompanyID=:CompanyID)) AS TipShare
			
			FROM HR_CleanPunch mcp
				-- Join store to determine the tip pool id
				-- Group by employeenum as we want the employee's tip share percentage
				-- as one row per employee
				JOIN Store ms ON mcp.StoreID = ms.StoreID
				WHERE ms.CompanyID = :CompanyID -- Parameter 1
				AND mcp.PayPeriod = :PayPeriod -- Parameter 2
				AND mcp.EmployeeNum NOT IN
						(SELECT EmployeeNum FROM HR_TerminatedEmployee WHERE CompanyID=:CompanyID)
				GROUP BY EmployeeNum, TipPoolID) cp

		ON s.TipPoolID = cp.TipPoolID
		JOIN HR_Tips t ON t.StoreID = s.StoreID
		
		WHERE t.PayPeriod = :PayPeriod -- Parameter 3
		AND COALESCE(t.Tips * cp.TipShare, 0) > 0""")

	tips = g.engine.execute(query, CompanyID=CompanyID, PayPeriod=PayPeriod).fetchall()

	response = make_response(render_template('export_paylocity_csv.html', hours=hours, tips=tips))
	response.mimetype = 'text/csv'
	response.headers['Content-Disposition'] = 'attachment; filename=%s_%s.csv' % (CompanyID, PayPeriod)
	return response

##### STORES ######

def valid_store_id(StoreID):
	#if re.match('^\d\d\d\d-\d\d\d-\d\d$', StoreID) is None:
	#	return False
	#else:
	#	return True

	return True

@app.route('/stores/edit/<StoreID>', methods=['GET', 'POST'])
@login_required
def store_edit(StoreID):
	if request.method == 'GET':
		store = g.t.Store.select().where(g.t.Store.c.StoreID==StoreID).execute().fetchone()

		companies = g.t.Company.select().order_by(g.t.Company.c.CompanyID).execute().fetchall()
		tippools = g.t.HR_TipPool.select().order_by(g.t.HR_TipPool.c.TipPoolID).execute().fetchall()

		return render_template('store_edit.html', store=store, companies=companies, tippools=tippools)

	if request.method == 'POST':
		if not valid_store_id(request.form['StoreID']):
			flash("Accounting ID format is invalid.", "alert-error")
			return redirect(url_for('store_edit', StoreID=StoreID))
		else:
			try:
				g.t.Store.update().where(g.t.Store.c.StoreID==StoreID).values(
					StoreID=request.form['StoreID'],
					Name=request.form['Name'],
					CompanyID=request.form['CompanyID'],
					TipPoolID=request.form['TipPoolID'],
					Costcenter=request.form['Costcenter'],
					Email=request.form['Email'],
					CtuitStoreID=request.form['CtuitStoreID']).execute()

				flash("Store %s updated successfully." % request.form['Name'], "alert-success")
				return redirect(url_for('stores'))

			except:
				flash("There was an unknown error updating the database. %s" % sys.exc_info()[0], "alert-error")
				return redirect(url_for('store_edit', StoreID=StoreID))

@app.route('/stores/add', methods=['GET', 'POST'])
@login_required
def store_add():
	if request.method == 'GET':
		companies = g.t.Company.select().order_by(g.t.Company.c.CompanyID).execute().fetchall()
		tippools = g.t.HR_TipPool.select().order_by(g.t.HR_TipPool.c.TipPoolID).execute().fetchall()
		return render_template('store_add.html', companies=companies, tippools=tippools)
	else:
		if not valid_store_id(request.form['StoreID']):
			flash("Accounting ID format is invalid.", "alert-error")
			return redirect(url_for('store_add'))
		else:
			try:
				g.t.Store.insert().values(
					StoreID=request.form['StoreID'],
					Name=request.form['Name'],
					CompanyID=request.form['CompanyID'],
					TipPoolID=request.form['TipPoolID'],
					Costcenter=request.form['Costcenter'],
					Email=request.form['Email'],
					CtuitStoreID=request.form['CtuitStoreID']).execute()

				flash("Store %s added successfully." % request.form['Name'], "alert-success")
				return redirect(url_for('stores'))
			except:
				flash("There was an unknown error updating the database. %s" % sys.exc_info()[0], "alert-error")
				return redirect(url_for('store_add'))

@app.route('/stores/activate/<StoreID>')
def store_activate(StoreID):
	try:
		g.t.Store.update().where(g.t.Store.c.StoreID==StoreID).values(
			Active=1).execute()

		flash("Store %s activated successfully." % StoreID, "alert-success")
		return redirect(url_for('stores'))
	except:
		flash("There was an unknown error updating the database. %s" % sys.exc_info()[0], "alert-error")
		return redirect(url_for('stores'))

@app.route('/stores/deactivate/<StoreID>')
def store_deactivate(StoreID):
	try:
		g.t.Store.update().where(g.t.Store.c.StoreID==StoreID).values(
			Active=0).execute()

		flash("Store %s deactivated successfully." % StoreID, "alert-success")
		return redirect(url_for('stores'))
	except:
		flash("There was an unknown error updating the database. %s" % sys.exc_info()[0], "alert-error")
		return redirect(url_for('stores'))

@app.route('/stores/delete/<StoreID>')
@login_required
def store_delete(StoreID):
	try:
		g.t.Store.delete().where(g.t.Store.c.StoreID==StoreID).execute()

		flash("Store %s deleted successfully." % StoreID, "alert-success")
		return redirect(url_for('stores'))
	except:
		flash("There was an unknown error updating the database. %s" % sys.exc_info()[0], "alert-error")
		return redirect(url_for('store_edit', StoreID=StoreID))

@app.route('/stores')
@login_required
def stores():
	query = text("""SELECT Store.*, Company.Name AS CompanyName, HR_TipPool.Name AS TipPoolName FROM Store
		LEFT JOIN Company ON Store.CompanyID = Company.CompanyID
		LEFT JOIN HR_TipPool ON Store.TipPoolID = HR_TipPool.TipPoolID
		ORDER BY Store.Active DESC, StoreID""")

	return render_template('stores.html', stores=g.engine.execute(query))



##### JOBS ######

@app.route('/swirl/jobs/add', methods=['GET', 'POST'])
@login_required
def job_add():
	if request.method == 'GET':
		return render_template('job_add.html')
	else:
		try:
			if 'Tipped' in request.form and request.form['Tipped'] == "on":
				Tipped = True
			else:
				Tipped = False
		
			g.t.HR_Job.insert().values(
				Name=request.form['Name'],
				Tipped=Tipped,
				Ratecode=request.form['Ratecode']).execute()

			flash("Job %s added successfully." % request.form['Name'], "alert-success")
			return redirect(url_for('jobs'))
		except:
			flash("Unknown error adding job %s." % request.form['Name'], "alert-error")
			return render_template('job_add.html')

@app.route('/swirl/jobs/edit/<int:JobID>', methods=['GET', 'POST'])
@login_required
def job_edit(JobID):
	if request.method == 'GET':
		return render_template('job_edit.html',
			job=g.t.HR_Job.select().where(g.t.HR_Job.c.JobID==JobID).execute().fetchone())
	else:
		try:
			if 'Tipped' in request.form and request.form['Tipped'] == "on":
				Tipped = True
			else:
				Tipped = False

			g.t.HR_Job.update().values(
				Name=request.form['Name'],
				Tipped=Tipped,
				Ratecode=request.form['Ratecode']).where(g.t.HR_Job.c.JobID==JobID).execute()

			flash("Job %s updated successfully." % request.form['Name'], "alert-success")
			return redirect(url_for('jobs'))
		except:
			flash("Unknown error updating job %s." % request.form['Name'], "alert-error")
			return redirect(url_for('job_edit', JobID))

@app.route('/swirl/jobs')
@login_required
def jobs():
	return render_template('jobs.html',
		jobs=g.t.HR_Job.select().order_by(g.t.HR_Job.c.JobID).execute())

@app.route('/swirl/jobs/delete/<int:JobID>')
@login_required
def job_delete(JobID):
	try:
		g.t.HR_Job.delete().where(g.t.HR_Job.c.JobID==JobID).execute()

		flash("Job ID %d deleted successfully." % JobID, "alert-success")
		return redirect(url_for('jobs'))
	except:
		flash("Error deleting job ID %d." % JobID, "alert-error")
		return redirect(url_for('jobs'))



##### PAY PERIODS #####

@app.route('/swirl/payperiods/add')
@login_required
def payperiod_add():
	try:
		# Get the most recent added pay period
		payperiod = g.t.HR_PayPeriod.select().order_by(g.t.HR_PayPeriod.c.PayPeriod.desc()).limit(1).execute().fetchone()

		# Add half a month to the pay period, and then adjust to either day 1 or day 16
		date = payperiod['PayPeriod']
		delta = datetime.timedelta(days=17)
		date = date + delta
		if date.day > 15:
			date = date.replace(day=16)
		else:
			date = date.replace(day=1)

		# Insert into database
		g.t.HR_PayPeriod.insert().values(PayPeriod=date.strftime('%Y-%m-%d')).execute()

		flash("New pay period added successfully.", "alert-success")

		return redirect(url_for('payperiods'))
	except:
		flash("Unable to add new pay period.", "alert-error")
		return redirect(url_for('payperiods'))

@app.route('/swirl/payperiods')
@login_required
def payperiods():
	return render_template('payperiods.html',
		payperiods=g.t.HR_PayPeriod.select().order_by(g.t.HR_PayPeriod.c.PayPeriod.desc()).execute())



##### TIP POOLS #####

@app.route('/swirl/tippools')
@login_required
def tippools():
	return render_template('tippools.html',
		tippools=g.t.HR_TipPool.select().order_by(g.t.HR_TipPool.c.TipPoolID).execute())

@app.route('/swirl/tippools/add', methods=['POST'])
@login_required
def tippool_add():
	try:
		g.t.HR_TipPool.insert().values(Name=request.form['Name']).execute()

		flash("Tip pool %s added successfully." % request.form['Name'], "alert-success")
		return redirect(url_for('tippools'))
	except:
		flash("Error adding tip pool %s." % request.form['Name'], "alert-error")
		return redirect(url_for('tippools'))

@app.route('/swirl/tippools/delete/<int:TipPoolID>')
@login_required
def tippool_delete(TipPoolID):
	try:
		g.t.HR_TipPool.delete().where(g.t.HR_TipPool.c.TipPoolID==TipPoolID).execute()

		flash("Tip pool ID %d deleted successfully." % TipPoolID, "alert-success")
		return redirect(url_for('tippools'))
	except:
		flash("Error deleting tip pool ID %d." % TipPoolID, "alert-error")
		return redirect(url_for('tippools'))



##### TIPS #####

@app.route('/swirl/tips/delete/<PayPeriod>/<StoreID>')
@login_required
def tips_delete(PayPeriod, StoreID):
	try:
		query = text('DELETE FROM HR_Tips WHERE PayPeriod = :pay_period AND StoreID = :store_id')
		g.engine.execute(query, pay_period=PayPeriod, store_id=StoreID)

		flash("Tips for pay period %s and Store %s deleted." % (PayPeriod, StoreID), "alert-success")
		return redirect(url_for('tips_pp', PayPeriod=PayPeriod))
	except:
		flash("Tips for pay period %s and Store %s couldn't be deleted." % (PayPeriod, StoreID), "alert-error")
		return redirect(url_for('tips_pp'), PayPeriod=PayPeriod)

@app.route('/swirl/tips/add', methods=['POST'])
@login_required
def tips_add():
	try:
		g.t.HR_Tips.insert().values(
			PayPeriod=request.form['PayPeriod'],
			StoreID=request.form['StoreID'],
			Tips=request.form['Tips']).execute()

		flash("Tips for pay period %s and Store %s added." % (request.form['PayPeriod'],
			request.form['StoreID']), "alert-success")
		return redirect(url_for('tips_pp', PayPeriod=request.form['PayPeriod']))
	except:
		flash("Tips for pay period %s and Store %s couldn't be added." % (request.form['PayPeriod'],
			request.form['StoreID']), "alert-error")
		return redirect(url_for('tips_pp', PayPeriod=request.form['PayPeriod']))

@app.route('/swirl/tips/update', methods=['POST'])
@login_required
def tips_update():
	try:
		g.engine.execute(
			text("UPDATE HR_Tips SET Tips = :Tips WHERE StoreID=:StoreID AND PayPeriod=:PayPeriod"),
			Tips=request.form['Tips'],
			StoreID=request.form['StoreID'],
			PayPeriod=request.form['PayPeriod'])

		flash("Tips updated for %s updated to %s." % (request.form['StoreID'], request.form['Tips']), 'alert-success')
		return redirect(url_for('tips_pp', PayPeriod=request.form['PayPeriod']))
	except:
		flash("Error updating the tips.", 'alert-error')
		return redirect(url_for('tips_pp', PayPeriod=request.form['PayPeriod']))

@app.route('/swirl/company/activate/<company_id>')
@login_required
def company_activate(company_id):
	try:
		g.engine.execute(text('UPDATE Company SET Active=1 WHERE CompanyID=:CompanyID'), CompanyID=company_id)
		flash("%s activated!" % company_id, 'alert-success')
	except:
		flash("There was an error!", 'alert-error')

	return(redirect(url_for('companies')))

@app.route('/swirl/company/deactivate/<company_id>')
@login_required
def company_deactivate(company_id):
	try:
		g.engine.execute(text('UPDATE Company SET Active=0 WHERE CompanyID=:CompanyID'), CompanyID=company_id)
		flash("%s deactivated!" % company_id, 'alert-success')
	except:
		flash("There was an error!", 'alert-error')

	return(redirect(url_for('companies')))

@app.route('/swirl/tips/<PayPeriod>')
@login_required
def tips_pp(PayPeriod):
	# Retrieve all of the pay periods in reverse date order for the pay period combo
	payperiods = g.t.HR_PayPeriod.select().order_by(g.t.HR_PayPeriod.c.PayPeriod.desc()).execute()

	# Retrieve the listing of tip amounts for PayPeriod
	query = text(
		"""SELECT HR_Tips.StoreID, Store.Name AS StoreName, HR_Tips.Tips FROM HR_Tips
		JOIN Store ON HR_Tips.StoreID = Store.StoreID
		WHERE HR_Tips.PayPeriod = :PayPeriod AND Store.Active = 1
		ORDER BY HR_Tips.StoreID""")
	tips = g.engine.execute(query, PayPeriod=PayPeriod)

	# Retrieve all the stores for this pay period that don't yet have any tips
	stores = g.engine.execute(
		text("""SELECT StoreID, Name FROM Store
			WHERE Store.Active = 1 AND
			Store.StoreID NOT IN
			(SELECT HR_Tips.StoreID FROM HR_Tips WHERE HR_Tips.PayPeriod = :PayPeriod)
		ORDER BY StoreID"""), PayPeriod=PayPeriod)

	# Convert the PayPeriod into a datetime.date so that it can be properly compared
	# in the template when trying to select the correct combo box item
	(y, m, d) = PayPeriod.split('-')
	pp = datetime.date(int(y), int(m), int(d))

	return render_template('tips.html', payperiod=pp, payperiods=payperiods, tips=tips, stores=stores)

@app.route('/swirl/tips')
@login_required
def tips():
	# Get most recent pay period and then default to that
	pp = g.t.HR_PayPeriod.select().order_by(g.t.HR_PayPeriod.c.PayPeriod.desc()).limit(1).execute().fetchone()

	return redirect(url_for('tips_pp', PayPeriod=pp['PayPeriod']))




##### JOB MAP ######

@app.route('/swirl/jobmap/delete/<StoreID>/<SquirrelJob>')
@login_required
def jobmap_delete(StoreID, SquirrelJob):
	try:
		query = text('DELETE FROM HR_JobMap WHERE StoreID=:store_id AND SquirrelJob=:squirrel_job')

		g.engine.execute(query, store_id=StoreID, squirrel_job=SquirrelJob)

		flash("Job map deleted successfully.", "alert-success")
	except:
		flash("Error while deleting job map.", "alert-error")

	return redirect(url_for('jobmap_store', StoreID=StoreID))

@app.route('/swirl/jobmap/add', methods=['POST'])
@login_required
def jobmap_add():
	try:
		g.t.HR_JobMap.insert().values(
			StoreID=request.form['StoreID'],
			SquirrelJob=request.form['SquirrelJob'],
			JobID=request.form['JobID']).execute()

		flash("Job map added successfully.", "alert-success")
	except:
		flash("Error while adding job map.", "alert-error")

	return redirect(url_for('jobmap_store', StoreID=request.form['StoreID']))	

@app.route('/swirl/jobmap/<StoreID>')
@login_required
def jobmap_store(StoreID):
	# Retrieve a list of stores
	stores = g.t.Store.select().order_by(g.t.Store.c.StoreID).execute()

	# Retrieve the current Store
	current_store = g.t.Store.select().where(g.t.Store.c.StoreID==StoreID).execute().fetchone()

	# Retrieve a list of available jobs
	jobs = g.t.HR_Job.select().order_by(g.t.HR_Job.c.JobID).execute()

	# Retrieve a list of job maps applicable to the currently selected Store
	query = text("""SELECT HR_JobMap.StoreID, HR_JobMap.SquirrelJob, Store.Name AS StoreName,
		HR_Job.Name AS JobName FROM HR_JobMap
		JOIN Store ON HR_JobMap.StoreID = Store.StoreID
		JOIN HR_Job ON HR_JobMap.JobID = HR_Job.JobID
		WHERE HR_JobMap.StoreID = :store_id
		ORDER BY HR_JobMap.SquirrelJob""")
	jobmaps = g.engine.execute(query, store_id=StoreID)

	return render_template('jobmap.html', stores=stores, current_store=current_store,
		jobs=jobs, jobmaps=jobmaps)

@app.route('/swirl/jobmap')
@login_required
def jobmap():
	Store = g.t.Store.select().order_by(g.t.Store.c.StoreID).limit(1).execute().fetchone()

	return redirect(url_for('jobmap_store', StoreID=Store['StoreID']))

# AJAX method to update job maps
@app.route('/swirl/_jobmap', methods=['POST'])
@login_required
def _jobmap():
	try:
		g.t.HR_JobMap.insert().values(
			StoreID=request.form['StoreID'],
			SquirrelJob=request.form['SquirrelJob'],
			JobID=request.form['JobID']).execute()

		return jsonify(SquirrelJob=request.form['SquirrelJob'], StoreID=request.form['StoreID'], result=True)
	except:
		return jsonify(SquirrelJob=request.form['SquirrelJob'], StoreID=request.form['StoreID'], result=False)


##### COMPANIES #####

@app.route('/companies')
@login_required
def companies():
	companies = g.t.Company.select().order_by(g.t.Company.c.CompanyID).execute()
	holiday_schedules = g.t.HR_HolidaySchedule.select().order_by(g.t.HR_HolidaySchedule.c.HolidayScheduleID).execute()

	return render_template('companies.html', companies=companies, holiday_schedules=holiday_schedules)

@app.route('/companies/add', methods=['POST'])
@login_required
def company_add():
	try:
		g.t.Company.insert().values(CompanyID=request.form['CompanyID'], Name=request.form['Name'], OvertimeRules=request.form['OvertimeRules'], HolidayScheduleID=request.form['HolidayScheduleID']).execute()

		flash("%s added successfully." % request.form['Name'], "alert-success")
	except:
		flash("Unknown error adding %s." % request.form['Name'], "alert-error")

	return redirect(url_for('companies'))

@app.route('/companies/edit/<CompanyID>', methods=['GET', 'POST'])
@login_required
def company_edit(CompanyID):
	if request.method == 'GET':
		company = g.t.Company.select().where(g.t.Company.c.CompanyID==CompanyID).execute().fetchone()
		holiday_schedules = g.t.HR_HolidaySchedule.select().execute().fetchall()

		return render_template('company_edit.html', company=company, holiday_schedules=holiday_schedules)
	elif request.method == 'POST':
		try:
			g.t.Company.update().where(g.t.Company.c.CompanyID==CompanyID).values(Name=request.form['Name'], CompanyID=request.form['CompanyID'], OvertimeRules=request.form['OvertimeRules'], HolidayScheduleID=request.form['HolidayScheduleID']).execute()

			flash('Company updated successfully.', 'alert-success')
		except:
			flash('There was an error updating the company.')

		return redirect(url_for('companies'))

@app.route('/companies/delete/<CompanyID>')
@login_required
def company_delete(CompanyID):
	try:
		g.t.Company.delete().where(g.t.Company.c.CompanyID==CompanyID).execute()

		flash("%s deleted successfully." % CompanyID, "alert-success")
	except:
		flash("Error deleting %s." % CompanyID, "alert-error")

	return redirect(url_for('companies'))





##### HOLIDAYS #####

@app.route('/swirl/holidays')
@login_required
def holidays():
	HolidaySchedule = g.t.HR_HolidaySchedule.select().order_by(g.t.HR_HolidaySchedule.c.HolidayScheduleID).limit(1).execute().fetchone()

	return redirect(url_for('holiday_schedule', HolidayScheduleID=HolidaySchedule['HolidayScheduleID']))

@app.route('/swirl/holidays/<int:HolidayScheduleID>')
@login_required
def holiday_schedule(HolidayScheduleID):
	holidays = g.t.HR_Holiday.select().where(g.t.HR_Holiday.c.HolidayScheduleID==HolidayScheduleID).order_by(g.t.HR_Holiday.c.Holiday.desc()).execute()
	holiday_schedules = g.t.HR_HolidaySchedule.select().order_by(g.t.HR_HolidaySchedule.c.HolidayScheduleID).execute()

	current_holiday_schedule = g.t.HR_HolidaySchedule.select().where(g.t.HR_HolidaySchedule.c.HolidayScheduleID==HolidayScheduleID).execute().fetchone()
	
	return render_template('holidays.html', holidays=holidays, holiday_schedules=holiday_schedules, current_holiday_schedule=current_holiday_schedule)

@app.route('/swirl/holidays/delete/<int:HolidayScheduleID>/<Holiday>')
@login_required
def holiday_delete(HolidayScheduleID, Holiday):
	g.t.HR_Holiday.delete().where(
		and_(
			g.t.HR_Holiday.c.Holiday==Holiday,
			g.t.HR_Holiday.c.HolidayScheduleID==HolidayScheduleID)).execute()

	flash("Holiday %s deleted successfully." % Holiday, "alert-success")

	return redirect(url_for('holidays'))

@app.route('/swirl/holidays/add', methods=['POST'])
@login_required
def holiday_add():
	try:
		g.t.HR_Holiday.insert().values(Holiday=request.form['Holiday'], Name=request.form['Name'], HolidayScheduleID=request.form['HolidayScheduleID']).execute()

		flash("Holiday %s added successfully." % request.form['Holiday'], "alert-success")
	except:
		flash("Error adding new holiday.", "alert-error")
	return redirect(url_for('holiday_schedule', HolidayScheduleID=request.form['HolidayScheduleID']))

##### HOLIDAYS #####

@app.route('/swirl/holiday_schedules')
@login_required
def holiday_schedules():
	holiday_schedules = g.t.HR_HolidaySchedule.select().order_by(g.t.HR_HolidaySchedule.c.HolidayScheduleID).execute()

	return render_template('holiday_schedules.html', holiday_schedules=holiday_schedules)

@app.route('/swirl/holiday_schedules/delete/<int:HolidayScheduleID>')
@login_required
def holiday_schedules_delete(HolidayScheduleID):
	try:
		g.t.HR_HolidaySchedule.delete().where(g.t.HR_HolidaySchedule.c.HolidayScheduleID==HolidayScheduleID).execute()
		flash("Holiday schedule deleted successfully.", 'alert-success')

	except:
		flash('Error deleting holiday schedule.', 'alert-error')

	return redirect(url_for('holiday_schedules'))

@app.route('/swirl/holiday_schedules/add', methods=['POST'])
@login_required
def holiday_schedules_add():
	try:
		g.t.HR_HolidaySchedule.insert().values(Name=request.form['Name']).execute()

		flash('Holiday schedule added successfully.', 'alert-success')
	except:
		flash('Error adding new holiday schedule.', 'alert-error')

	return redirect(url_for('holiday_schedules'))

@app.route('/swirl/terminatedee')
@login_required
def terminated_ee():
	return render_template('terminated_ee.html',
		ee=g.t.HR_TerminatedEmployee.select().execute().fetchall(), companies=g.t.Company.select().execute().fetchall())

@app.route('/swirl/terminatedee/delete/<CompanyID>/<EmployeeNum>')
@login_required
def terminated_ee_delete(CompanyID, EmployeeNum):
	g.engine.execute(text(
		"""DELETE FROM HR_TerminatedEmployee
		WHERE CompanyID = :CompanyID
		AND EmployeeNum = :EmployeeNum
		"""), CompanyID=CompanyID, EmployeeNum=EmployeeNum)

	return redirect(url_for('terminated_ee'))

@app.route('/swirl/terminatedee/add', methods=['POST'])
@login_required
def terminated_ee_add():
	try:
		g.t.HR_TerminatedEmployee.insert().values(CompanyID=request.form['CompanyID'],
			EmployeeNum=request.form['EmployeeNum']).execute()
		flash("Terminated EE added successfully.", "alert-success")
	except:
		flash("Error adding terminated EE.", "alert-error")

	return redirect(url_for('terminated_ee'))

# Run application from command line
if __name__ == '__main__':
	app.run()