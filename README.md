# Swirl

Swirl is an application custom written for Vino Volo's payroll process. Swirl imports punch reports in CSV format. It calculates tip pooling, overtime, and holiday pay and exports in a format that can be imported into Paylocity.